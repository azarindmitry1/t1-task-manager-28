package ru.t1.azarin.tm.exception.user;

public class IncorrectLoginOrPasswordException extends AbstractUserException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Login or password is incorrect...");
    }

}
